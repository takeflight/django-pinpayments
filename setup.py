#!/usr/bin/env python

from pinpayments import __version__
from setuptools import setup, find_packages


setup(
    name='django-pinpayments',
    version=__version__,
    description="Tools for easily adding pin.net.au payment processing to your Django proejct",
    long_description=open('README.rst', 'r').read(),
    classifiers=[
        "Programming Language :: Python",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Framework :: Django",
        "Environment :: Web Environment",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Intended Audience :: Developers",
        "Development Status :: 3 - Alpha",
    ],
    keywords=['django', 'payments', 'pinpayments',],
    author='Tim Heap',
    author_email='tim@takeflight.com.au',
    url='http://github.com/takeflight/django-pinpayments',
    license='BSD',

    packages=find_packages(),

    package_data={},  # Included via MANIFEST.in
    include_package_data=True,

    zip_safe=False,

    install_requires=[
        'requests>=2.3.0,<2.4',
        'South>=0.8.3,<0.9',
        'Django>=1.4,<1.8',
    ],
)

