from django.forms import widgets


class RequiredWidget(widgets.Widget):
    """
    Adds a required attribute to the field if it is required
    """
    def build_attrs(self, extra_attrs=None, **kwargs):
        if self.is_required:
            kwargs['required'] = 'required'
        return super(RequiredWidget, self).build_attrs(extra_attrs, **kwargs)


class NamelessWidget(widgets.Widget):
    """
    Removes the name attribute from a field
    """
    def build_attrs(self, extra_attrs=None, **kwargs):
        attrs = super(NamelessWidget, self).build_attrs(extra_attrs, **kwargs)
        attrs['data-name'] = attrs.pop('name', '')
        return attrs


class PinHiddenInput(widgets.HiddenInput, RequiredWidget, NamelessWidget):
    pass


class PinTextInput(widgets.TextInput, RequiredWidget, NamelessWidget):
    pass


class PinSelect(widgets.Select, RequiredWidget, NamelessWidget):
    pass
