(function($) {
	"use strict";
	var pluginName = 'pinForm';
	var eNS = '.' + pluginName;

	var plugin = function(options) {
		$(this).each(function() {
			var $el = $(this);
			new PinForm($el, options);
		});
	};

	$.fn[pluginName] = plugin;

	plugin.defaultOptions = {
		formSelector: 'form',

		tokenSelector: 'input[data-pin-token]',
		ipAddressSelector: 'input[data-pin-ip-address]',

		errorsSelector: '[data-pin-error]',
		submitSelector: ':submit',
	};

	var PinForm = plugin.PinForm = function($el, options) {
		this.options = $.extend({}, plugin.defaultOptions, options);

		this.$root = $el;
		this.$form = $el.closest('form');
		this.$errors = $el.find(this.options.errorsSelector).hide();
		this.$submitButton = this.$form.find(this.options.submit);

		this.onsubmit = jQuery.proxy(this, 'onsubmit');
		this.handlePinResponse = jQuery.proxy(this, 'handlePinResponse');
		this.attach();
	};

	PinForm.prototype.attach = function() {
		this.$form.bind('submit' + eNS, this.onsubmit);
	};

	PinForm.prototype.detach = function() {
		this.$form.unbind('submit' + eNS, this.onsubmit);
	};

	PinForm.prototype.onsubmit = function(e) {
		e.preventDefault();
		this.$errors.hide();

		// Disable the submit button to prevent multiple clicks
		this.$submitButton.attr({disabled: true});

		// Fetch details required for the createToken call to Pin
		var card = {
			number: this.$root.find('[data-name=credit_card_number]').val(),
			name: this.$root.find('[data-name=credit_card_name]').val(),
			expiry_month: this.$root.find('[data-name=credit_card_expiry_month]').val(),
			expiry_year: this.$root.find('[data-name=credit_card_expiry_year]').val(),
			cvc: this.$root.find('[data-name=credit_card_security_code]').val(),
			address_line1: this.$root.find('[data-name=address_1]').val(),
			address_line2: this.$root.find('[data-name=address_2]').val(),
			address_city: this.$root.find('[data-name=city]').val(),
			address_state: this.$root.find('[data-name=state]').val(),
			address_postcode: this.$root.find('[data-name=postcode]').val(),
			address_country: this.$root.find('[data-name=country]').val()
		};

		// Request a token for the card from Pin
		Pin.createToken(card, this.handlePinResponse);
	};

	PinForm.prototype.handlePinResponse = function(response) {

		if (response.response) {
			// Add the card token and ip address of the customer to the form
			// You will need to post these to Pin when creating the charge.
			var options = this.options;
			this.$root.find(options.tokenSelector).val(response.response.token);
			this.$root.find(options.ipAddressSelector).val(response.ip_address);

			// Actually submit the form, after detaching our event handlers
			this.detach();
			this.$form.submit();

		} else {
			// Bummer
			this.setErrors(response.error_description, response.messages);
			this.$submitButton.removeAttr('disabled');
		}
	};

	PinForm.prototype.setErrors = function(description, messages) {
		this.$root.find('[data-pin-error-description]').text(description);
		var $errorList = this.$root.find('[data-pin-error-list]');
		$errorList.empty();

		if (messages) {
			$.each(messages, function(index, errorMessage) {
				$('<li>')
					.text(errorMessage.message)
					.appendTo($errorList);
			});
		}

		this.$errors.show();
	};
})(jQuery);
