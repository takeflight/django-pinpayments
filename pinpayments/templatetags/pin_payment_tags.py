from django import template
from django.template.loader import render_to_string

from ..conf import settings
from ..forms import PrintableTokenForm

register = template.Library()


@register.inclusion_tag('pinpayments/pin_headers.html')
def pinheader(environment=settings.PIN_DEFAULT_ENVIRONMENT):
    """
    pin_header - Renders the JavaScript required for Pin.js payments.
    This will also include the Pin.js file from pin.net.au.
    Optionally accepts an 'environment' (eg test/live) as a paramater,
    otherwise the default will be used.
    """
    if environment not in settings.PIN_ENVIRONMENTS:
        raise template.TemplateSyntaxError("Environment '%s' does not exist in PIN_ENVIRONMENTS" % environment)

    pin_env = settings.PIN_ENVIRONMENTS[environment]

    pin_key = pin_env.get('key', None)
    pin_host = pin_env.get('host', None)

    if not (pin_key and pin_host):
        raise template.TemplateSyntaxError("Environment '%s' does not have key and host configured." % environment)

    return {
        'STATIC_URL': settings.STATIC_URL,
        'MEDIA_URL': settings.MEDIA_URL,
        'pin_environment': environment,
        'pin_public_key': pin_key,
        'pin_host': pin_host,
    }


@register.simple_tag()
def pinform(template='pinpayments/pin_form.html'):
    """
    pin_form - renders a simple HTML form
    Should be inside existing <form class='pin'>...</form> tags.
    """
    return render_to_string(template, {
        'STATIC_URL': settings.STATIC_URL,
        'MEDIA_URL': settings.MEDIA_URL,
        'form': PrintableTokenForm()
    })


class InlinePinForm(template.Node):
    TAG_NAME = 'inlinepinform'
    END_TAG_NAME = 'end' + TAG_NAME

    def __init__(self, node_list):
        self.node_list = node_list

    def render(self, context):
        with context.push():
            context.update({
                'pin_form': PrintableTokenForm(),
            })
            return self.node_list.render(context)

    @classmethod
    def compile(cls, parser, token):
        node_list = parser.parse([cls.END_TAG_NAME])
        parser.delete_first_token()
        return cls(node_list)
register.tag(name=InlinePinForm.TAG_NAME)(InlinePinForm.compile)
