from django import forms
from django.utils import timezone

from .models import CustomerToken
from .widgets import PinSelect, PinTextInput


def years():
    current_year = timezone.now().year
    years = list(range(current_year, current_year + 15))
    return zip(years, years)


months = [
    ('01', "01 - January"),
    ('02', "02 - February"),
    ('03', "03 - March"),
    ('04', "04 - April"),
    ('05', "05 - May"),
    ('06', "06 - June"),
    ('07', "07 - July"),
    ('08', "08 - August"),
    ('09', "09 - September"),
    ('10', "10 - October"),
    ('11', "11 - November"),
    ('12', "12 - December"),
]



class TokenForm(forms.Form):
    """
    Credit card token form.
    Only the card token and IP address returned by PIN should be submitted,
    the rest is handled by Pin.js.
    """
    token = forms.CharField(
        widget=forms.HiddenInput(attrs={'data-pin-token': ''}))
    ip_address = forms.CharField(
        widget=forms.HiddenInput(attrs={'data-pin-ip-address': ''}))


class PrintableTokenForm(forms.Form):
    """
    A form with all the fields required for obtaining a credit card token,
    set up so that they will not be submitted in POST data.
    It is used by the ``{% pin_form %}`` template tag.
    This form can not be used for validation,
    as the validation is done client side
    """

    address_1 = forms.CharField(
        max_length=255, required=True, widget=PinTextInput)
    address_2 = forms.CharField(
        max_length=255, required=False, widget=PinTextInput)
    city = forms.CharField(
        max_length=255, required=True, widget=PinTextInput)
    state = forms.CharField(
        max_length=255, required=True, widget=PinTextInput)
    postcode = forms.CharField(
        max_length=50, required=True, widget=PinTextInput)
    country = forms.CharField(
        max_length=100, required=True, widget=PinTextInput)

    credit_card_number = forms.CharField(
        label="Credit card number", widget=PinTextInput)
    credit_card_name = forms.CharField(
        label="Name on card", widget=PinTextInput)
    credit_card_expiry_year = forms.ChoiceField(
        label="Card expiry year", choices=(), widget=PinSelect)
    credit_card_expiry_month = forms.ChoiceField(
        label="Card expiry month", choices=months, widget=PinSelect)
    credit_card_security_code = forms.CharField(
        label="Card security code", max_length=4, widget=PinTextInput)

    def __init__(self, *args, **kwargs):
        super(PrintableTokenForm, self).__init__(*args, **kwargs)
        self.fields['credit_card_expiry_year'].choices = years()
