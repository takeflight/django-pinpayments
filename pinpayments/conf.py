from django.conf import settings as base_settings

class SettingsWrapper(object):
    """
    A wrapper around the Django settings object, allowing us to set default
    settings for this app.
    """

    def __init__(self, **kwargs):
        self.settings = kwargs

    def __getattr__(self, attr, *default):
        try:
            return getattr(base_settings, attr)
        except AttributeError:
            own_settings = object.__getattribute__(self, 'settings')
            if attr in own_settings:
                return own_settings[attr]
            else:
                raise


settings = SettingsWrapper(
    PIN_DEFAULT_ENVIRONMENT='test',
    PIN_ENVIRONMENTS=None,
)
